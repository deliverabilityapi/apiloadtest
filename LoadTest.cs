﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ThreadState = System.Threading.ThreadState;
using Timer = System.Timers.Timer;

namespace ApiLoadTest
{
    public class LoadTest
    {
        private static int _timerIntervalProcessedCount = 0;
        private static int _timerIntervalErrorsCount = 0;
        private static int _timerTotalProcessedCount = 0;
        private static int _timerTotalErrorsCount = 0;
        private static readonly int _timerSecondInterval = 10;
        private static readonly object _timerWriteLockObject = new object();
        private static readonly object _fileWriteLockObject = new object();
        private static ConcurrentBag<int> _responseTimeBag;
        private static int _runningThreadCount = 0;

        private static Action<LoadTestNotification> _consoleWriteNotificationHandler = notification =>
        {
            Console.WriteLine(notification.NotificationTime + ": " + notification.Message);
        };

        public static async Task<bool> Run(int testSize, int threadCount, bool saveOutput, Action<LoadTestNotification> notificationHandler, CancellationToken cancellationToken)
        {
            if (notificationHandler == null)
                notificationHandler = _consoleWriteNotificationHandler;

            notificationHandler(new LoadTestNotification($"Starting load test (test_size = {testSize}, thread_count = {threadCount})..."));

            var testTimer = Stopwatch.StartNew();
            _timerIntervalProcessedCount = _timerIntervalErrorsCount = _timerTotalProcessedCount = _timerTotalErrorsCount = _runningThreadCount = 0;
            _responseTimeBag = new ConcurrentBag<int>();

            ServicePointManager.DefaultConnectionLimit = 200;

            var debugFile = saveOutput ? File.CreateText("debug.txt") : null;
            try
            {
                // populate a thread-safe bag of zip codes, randomly selected WITH replacement
                var zipCodeBag = new ConcurrentBag<string>();
                var zipCodeList = new InMemoryZipCodeCentroidProvider().Select(centroid => centroid.Key).ToList();
                var rand = new Random();
                for (var i = 0; i < testSize; i++)
                    zipCodeBag.Add(zipCodeList[rand.Next(zipCodeList.Count)]);

                // forecast cache
                var threadList = new List<Thread>();
                using (var outputTimer = new Timer(1000 * _timerSecondInterval))
                {
                    // setup our debug rate display timer
                    SetupOutputTimer(outputTimer, zipCodeBag, notificationHandler);
                    outputTimer.Start();

                    // set up each thread
                    for (var i = 0; i < threadCount; i++)
                    {
                        threadList.Add(new Thread(() =>
                        {
                            try
                            {
                                // keep grabbing zip codes from our global supply until we run out
                                var threadRandom = new Random();
                                string zipCode = null;
                                while (zipCodeBag.TryTake(out zipCode))
                                {
                                    var originZipCode = zipCodeList[threadRandom.Next(zipCodeList.Count)];
                                    try
                                    {
                                        var url = "https://deliverability.parcelshield.com/api/Deliverability";
                                        var post = WebRequest.CreateHttp(url);
                                        post.Method = "POST";
                                        post.ContentType = "application/json";
                                        var content =
                                            Encoding.UTF8.GetBytes("{\"Service\":" +
                                                (threadRandom.Next(12) + 1) +
                                                ",\"Carrier\":" +
                                                threadRandom.Next(3) +
                                                ",\"OriginDateTime\":\"" +
                                                DateTime.Now.AddHours(5 + threadRandom.Next(12)) +
                                                "\",\"OriginPostalCode\":\"" +
                                                originZipCode +
                                                "\",\"DestinationPostalCode\":\"" +
                                                zipCode +
                                                "\"}");
                                        post.Headers.Add("x-api-key", "d|K2#d~AOa!#@$/$](?};/lue+{Y;$rp_ATRhFjm2I&)#]:(|E!0Q_FXm!Ao)UnF");
                                        post.ContentLength = content.Length;
                                        post.Proxy = null;
                                        post.KeepAlive = true;
                                        var httpRequestTimer = Stopwatch.StartNew();
                                        var dataStream = post.GetRequestStream();
                                        dataStream.Write(content, 0, content.Length);
                                        dataStream.Close();
                                        using (var response = post.GetResponse())
                                        {
                                            using (var stream = response.GetResponseStream())
                                            {
                                                _responseTimeBag.Add((int)httpRequestTimer.ElapsedMilliseconds);
                                                using (var reader = new StreamReader(stream))
                                                    reader.ReadToEnd();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        if (!(ex is ThreadAbortException) && !(ex.InnerException is ThreadAbortException))
                                        {
                                            notificationHandler(new LoadTestNotification($"Test error (zip code: {zipCode}): {ex}"));
                                            Interlocked.Increment(ref _timerIntervalErrorsCount);
                                        }
                                    }
                                    Interlocked.Increment(ref _timerIntervalProcessedCount);
                                }
                            }
                            catch (Exception ex)
                            {
                                if (!(ex is ThreadAbortException)  &&  !(ex.InnerException is ThreadAbortException))
                                    notificationHandler(new LoadTestNotification(ex.ToString()));
                            }
                        }));
                    }

                    // TODO: if a thread dies during operation for some reason we should rebuild and restart it
                    var cancel = false;
                    try
                    {
                        for (var i = 0; i < threadList.Count; i++)
                        {
                            threadList[i].IsBackground = true;  // set all threads to background threads since they will all be long-running
                            threadList[i].Start();
                            _runningThreadCount++;
                            cancellationToken.ThrowIfCancellationRequested();
                            await Task.Delay(100, cancellationToken);
                        }

                        // wait (asynchronously) on all threads to finish
                        var stillRunning = true;
                        while (stillRunning)
                        {
                            stillRunning = false;
                            for (var i = 0; i < threadList.Count; i++)
                            {
                                if (threadList[i].ThreadState == ThreadState.Running  ||  threadList[i].ThreadState == ThreadState.Background)
                                    stillRunning = true;
                            }
                            cancellationToken.ThrowIfCancellationRequested();
                            await Task.Delay(100, cancellationToken);
                        }
                        var rate = _timerTotalProcessedCount / testTimer.Elapsed.TotalSeconds;
                        notificationHandler(new LoadTestNotification($"Load test finished in {testTimer.Elapsed.TotalSeconds.ToString("0.00")}, total rate: {rate.ToString("0.000")}/second, total items: {_timerTotalProcessedCount}"));
                    }
                    catch (OperationCanceledException)
                    {
                        // cancel all threads and return
                        notificationHandler(new LoadTestNotification("Cancelling load test..."));
                        // ReSharper disable once MethodSupportsCancellation
                        await Task.Delay(10);  // allow the test notification to propogate before we finish; there are no more async methods and this gives the user better clues as to what is going on
                        for (var i = 0; i < threadList.Count; i++)
                        {
                            try { threadList[i].Abort(); }
                            catch (Exception) { /* swallow exceptions */ }
                        }
                        notificationHandler(new LoadTestNotification("Load test cancelled."));
                    }
                }
            }
            finally
            {
                debugFile?.Close();
            }
            return true;
        }

        private static void SetupOutputTimer(Timer debugOutputTimer, ConcurrentBag<string> zipCodeBag, Action<LoadTestNotification> notificationHandler)
        {
            debugOutputTimer.Elapsed += (sender, args) =>
            {
                var thisProcessedCount = _timerIntervalProcessedCount;
                var thisErrorCount = _timerIntervalErrorsCount;
                _timerTotalProcessedCount += thisProcessedCount;
                _timerTotalErrorsCount += thisErrorCount;

                // generate the rate
                var rate = thisProcessedCount / (double)_timerSecondInterval;
                var remainingCount = zipCodeBag.Count;
                var estimatedTimeRemaining = rate == 0 ? TimeSpan.MaxValue : TimeSpan.FromSeconds(remainingCount / rate);
                var remaining = $"{estimatedTimeRemaining.Hours} hours, {estimatedTimeRemaining.Minutes} minutes";

                // create a new response time ConcurrentBag as an atomic operation, but keep a reference to the old bag
                var oldBag = _responseTimeBag;
                var newBag = new ConcurrentBag<int>();
                Interlocked.Exchange(ref _responseTimeBag, newBag);

                // generate the average response time and 90% response time
                var responseTimeList = oldBag.ToArray();
                Array.Sort(responseTimeList);
                var ninetyPercentResponseTime = responseTimeList.Length == 0 ? 0 : responseTimeList[(int)(responseTimeList.Length * 0.9)];
                var responseTimeSumMs = 0;
                for (var i = 0; i < responseTimeList.Length; i++)
                    responseTimeSumMs += responseTimeList[i];
                var averageResponseTimeMs = responseTimeList.Length == 0 ? 0 : responseTimeSumMs / (double)responseTimeList.Length;

                // write our message
                var message = $"Threads: {_runningThreadCount}, avg. resp. time: {averageResponseTimeMs.ToString("0")}ms, 90% resp. time: {ninetyPercentResponseTime}ms, Rate: {rate.ToString("0.000")}/second, processed: {_timerTotalProcessedCount}, errors: {_timerTotalErrorsCount}, remaining: {zipCodeBag.Count} items, {remaining}";
                notificationHandler(new LoadTestNotification(DateTime.Now, _timerTotalProcessedCount, _timerTotalErrorsCount, rate, averageResponseTimeMs, ninetyPercentResponseTime, message, null));
                _timerIntervalProcessedCount = 0;
                _timerIntervalErrorsCount = 0;
            };
        }

        public struct LoadTestNotification
        {
            public DateTime NotificationTime;
            public int? TotalRequests;
            public int? TotalErrors;
            public double? RequestsPerSecond;
            public double? AverageResponseTimeMs;
            public double? NinetyPercentResponseTimeMs;
            public string Message;
            public Exception Exception;
            public LoadTestNotification(string message) : this(DateTime.Now, null, null, null, null, null, message, null) { }
            public LoadTestNotification(DateTime notificationTime, string message) : this(notificationTime, null, null, null, null, null, message, null) { }
            public LoadTestNotification(int? totalRequests, int? totalErrors, double? requestsPerSecond,
                double? averageResponseTimeMs, double? ninetyPercentResponseTimeMs, string message) :
                this(DateTime.Now, totalRequests, totalErrors, requestsPerSecond, averageResponseTimeMs, ninetyPercentResponseTimeMs, message, null)
            { }
            public LoadTestNotification(DateTime notificationTime, int? totalRequests, int? totalErrors,
                double? requestsPerSecond, double? averageResponseTimeMs, double? ninetyPercentResponseTimeMs, string message,
                Exception exception)
            {
                NotificationTime = notificationTime;
                TotalRequests = totalRequests;
                TotalErrors = totalErrors;
                RequestsPerSecond = requestsPerSecond;
                AverageResponseTimeMs = averageResponseTimeMs;
                NinetyPercentResponseTimeMs = ninetyPercentResponseTimeMs;
                Message = message;
                Exception = exception;
            }
        }
    }
}
