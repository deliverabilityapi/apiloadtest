﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ApiLoadTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ButtonSwitchTestState.Click += ButtonSwitchTestStateOnClick;
        }

        private static CancellationTokenSource _cancellationTokenSource;

        private async void ButtonSwitchTestStateOnClick(object sender, RoutedEventArgs routedEventArgs)
        {
            if (_cancellationTokenSource == null)
                await StartTest();
            else
                StopTest();
        }

        private void StopTest()
        {
            if (_cancellationTokenSource == null  ||  _cancellationTokenSource.IsCancellationRequested)
                return;
            _cancellationTokenSource.Cancel();
        }

        private async Task StartTest()
        {
            // parameter validation
            int threadCount = 0, testApiCalls = 0;
            if (!int.TryParse(TextBoxThreadCount.Text, out threadCount) ||
                !int.TryParse(TextBoxTestApiCalls.Text, out testApiCalls))
            {
                MessageBox.Show("Please specify a number for thread count and test API calls.");
                return;
            }
            if (threadCount <= 0 || threadCount > 100)
            {
                MessageBox.Show("Please specify a number between 1 and 100 for thread count.");
                return;
            }
            if (testApiCalls <= 0 || testApiCalls > 100000)
            {
                MessageBox.Show("Please specify a number between 1 and 100000 for test API calls.");
                return;
            }

            // start the test
            ButtonSwitchTestState.Content = "Stop Test";
            TextBoxTestResults.Text = "";
            try
            {
                _cancellationTokenSource = new CancellationTokenSource();
                await LoadTest.Run(testApiCalls, threadCount, false, NotificationHandler, _cancellationTokenSource.Token);
            }
            catch (TaskCanceledException)
            {
                // do nothing; expected
            }
            finally
            {
                _cancellationTokenSource = null;
                ButtonSwitchTestState.Content = "Start Test";
            }
        }

        private void AppendTestResults(string results)
        {
            TextBoxTestResults.AppendText(results + "\r\n");
            TextBoxTestResults.ScrollToEnd();
        }

        private void NotificationHandler(LoadTest.LoadTestNotification loadTestNotification)
        {
            // execute on the correct thread
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(() => AppendTestResults(loadTestNotification.Message), DispatcherPriority.Normal);
            else
                AppendTestResults(loadTestNotification.Message);
        }
    }
}
