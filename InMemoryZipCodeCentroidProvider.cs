﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ApiLoadTest
{
    public class InMemoryZipCodeCentroidProvider : IEnumerable<KeyValuePair<string, Coordinate>>
    {
        private static readonly string _ZIP_CODE_CENTROID_FILE = "zip_code_centroid.data";
        private static readonly Dictionary<string, Coordinate> _ZIP_CODE_CENTROID_MAP = new Dictionary<string, Coordinate>();

        static InMemoryZipCodeCentroidProvider()
        {
            var assembly = Assembly.GetExecutingAssembly();

            // the zip code centroid data is loaded as an assembly resource, load it in now
            var resourceName = assembly.GetManifestResourceNames().First(name => name.EndsWith(_ZIP_CODE_CENTROID_FILE));
            if (string.IsNullOrWhiteSpace(resourceName))
                return;
            var regionResourceStream = assembly.GetManifestResourceStream(resourceName);
            if (regionResourceStream == null)
                return;
            _ZIP_CODE_CENTROID_MAP = ReadZipCodeCentroidBinary(regionResourceStream);
        }

        public Coordinate Get(string zipCode)
        {
            return _ZIP_CODE_CENTROID_MAP[zipCode];
        }

        public virtual IEnumerator<KeyValuePair<string, Coordinate>> GetEnumerator()  // virtual for testing
        {
            return _ZIP_CODE_CENTROID_MAP.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

        #region READ/WRITE DATA FILE
        private static void WriteZipCodeCentroidBinary(string fileName, Dictionary<string, Coordinate> zipCodeCentroidMap)
        {
            using (var compressionStream = new DeflateStream(File.OpenWrite(fileName), CompressionLevel.Fastest))
            {
                using (var writer = new BinaryWriter(compressionStream))
                {
                    // NOTE: we always write in the current version
                    // write the header
                    writer.Write((uint)1);
                    writer.Write(zipCodeCentroidMap.Count);

                    // write each geoMap item
                    foreach (var zipCodeItem in zipCodeCentroidMap)
                    {
                        writer.Write(zipCodeItem.Key);
                        writer.Write(zipCodeItem.Value.Latitude);
                        writer.Write(zipCodeItem.Value.Longitude);
                    }
                }
            }
        }

        private static Dictionary<string, Coordinate> ReadZipCodeCentroidBinary(Stream stream)
        {
            using (var decompressionStream = new DeflateStream(stream, CompressionMode.Decompress))
            {
                using (var reader = new BinaryReader(decompressionStream))
                {
                    var zipCodeCentroidMap = new Dictionary<string, Coordinate>();

                    // read the header
                    var thisBinaryFileVersion = reader.ReadUInt32(); // unused for now; we are on the first version
                    var zipCodeCentroidCount = reader.ReadInt32();

                    // read each zip code centroid record
                    for (var i = 0; i < zipCodeCentroidCount; i++)
                    {
                        var zipCode = reader.ReadString();
                        zipCodeCentroidMap.Add(zipCode, new Coordinate(reader.ReadSingle(), reader.ReadSingle()));
                    }

                    return zipCodeCentroidMap;
                }
            }
        }
        #endregion
    }

    public class Coordinate : IComparable<Coordinate>
    {
        private readonly float[] _coordinate = new float[2];
        public float Latitude { get { return _coordinate[0]; } set { _coordinate[0] = value; } }
        public float Longitude { get { return _coordinate[1]; } set { _coordinate[1] = value; } }
        public Coordinate(float latitude, float longitude)
        {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

        // compare by latitude first, then longitude, so sorts on this object will be sorted by latitude first
        public int CompareTo(Coordinate that)
        {
            var latitudeComparison = this.Latitude.CompareTo(that.Latitude);
            return latitudeComparison != 0 ? latitudeComparison : this.Longitude.CompareTo(that.Longitude);
        }

        public override string ToString()
        {
            return this.Latitude.ToString() + "," + this.Longitude.ToString();
        }
    }

}
